class DataCovid2 {
  List<String> positif;
  List<String> sembuh;
  List<String> hari;

  DataCovid2({
    required this.positif,
    required this.sembuh,
    required this.hari,
  });

  factory DataCovid2.fromJson(Map<String, dynamic> json) {
    return DataCovid2(
        positif: json['positif'], sembuh: json['sembuh'], hari: json['hari']);
  }
}
