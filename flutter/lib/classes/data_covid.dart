import 'package:intl/intl.dart';

class DataCovid {
  String positif;
  String dirawat;
  String sembuh;
  String meninggal;

  DataCovid({
    required this.positif,
    required this.dirawat,
    required this.sembuh,
    required this.meninggal,
  });

  factory DataCovid.fromJson(Map<String, dynamic> json) {
    var formatter = NumberFormat('#,###,000');
    return DataCovid(
        positif: formatter.format(json['jumlah_positif']),
        dirawat: formatter.format(json['jumlah_dirawat']),
        sembuh: formatter.format(json['jumlah_sembuh']),
        meninggal: formatter.format(json['jumlah_meninggal']));
  }
}
