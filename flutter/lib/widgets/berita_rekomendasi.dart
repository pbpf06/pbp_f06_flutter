import '../screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/main.dart' show Env;
import 'dart:convert';
import 'package:pbp_f06/constants.dart';

class BeritaRekomendasi extends StatefulWidget {
  const BeritaRekomendasi({ Key? key }) : super(key: key);

  @override
  _BeritaRekomendasiState createState() => _BeritaRekomendasiState();
}

class _BeritaRekomendasiState extends State<BeritaRekomendasi> {

  Widget? lists;
  bool lock=false;
  
  void initLists() async
  {
    if (lock)
    {
      return;
    }
    lock = true;//acquire lock, unable to get request for a while
    var response = await http.get(Uri.parse("${Env.URL_PREFIX}/berita/rekomendasi"));
    if (response.statusCode==200)
    {
      Map<String, dynamic> map = jsonDecode(response.body);
      List<Widget> result = <Widget>[];
      for (dynamic data in map["data"])
      {
        result.add(
                Card(  
              child: Column(  
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[  
                  ListTile(  
                    title: Text(data["title"]),  
                    subtitle: Text(data["author"]! +"\n"+data["date"]!),
                    isThreeLine: true,
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder:  (context) => BeritaView(
                          title: data["title"], author:data["author"], id:data["id"]
                          )));
                    }
                  ),  
                ],  
              ),  
            )
          );
      }
      setState(() 
      {
        lists = Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: result);
      });
    }
    else
    {
      setState((){
        lists = Center(
          child: Text("An error has occurred")
        );
      });
    }
    lock = false;
  }

  @override
  Widget build(BuildContext context) {
    if (lists==null)
    {
      initLists();
    }
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
        decoration: BoxDecoration(
          border: Border.all(width: 1),
          borderRadius: const BorderRadius.all(Radius.circular(9)),
          color: const Color(0xFF0D8E53),
        ),
        child: Column(
          children: [
              Center(
                child: Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Text("Berita lainnya",
                  style: TextStyle(color: Color(0xFFACB1C0), fontSize: 30),
                )),
              ),
              lists ?? Center(child: CircularProgressIndicator())
              ]
            )
    );
  }
}