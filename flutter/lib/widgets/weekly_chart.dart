import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import 'package:pbp_f06/main.dart';

class WeeklyChart extends StatefulWidget {
  const WeeklyChart({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => WeeklyChartState();
}

class WeeklyChartState extends State<WeeklyChart> {
  final Color leftBarColor = kPrimaryColor;
  final Color rightBarColor = const Color(0xffff5182);
  final double width = 7;

  late List<BarChartGroupData> rawBarGroups;
  late List<BarChartGroupData> showingBarGroups;

  List<String> positif = Env.dataCovid2.positif;
  List<String> sembuh = Env.dataCovid2.sembuh;

  List<double> newPositif = [];
  List<double> newSembuh = [];
  double max = 0;
  double secondMax = 0;
  double thirdMax = 0;

  int touchedGroupIndex = -1;

  @override
  void initState() {
    super.initState();

    for (int i = 0; i < positif.length; i++) {
      newPositif.add(double.parse(positif[i]));
      if (double.parse(positif[i]) > max) {
        max = double.parse(positif[i]);
      }
      newSembuh.add(double.parse(sembuh[i]));
      if (double.parse(sembuh[i]) > max) {
        max = double.parse(sembuh[i]);
      }
    }

    while (max % 100 != 0) {
      max++;
    }

    secondMax = max / 2;

    for (int i = 0; i < positif.length; i++) {
      newPositif[i] = newPositif[i] / max * 20;
      newSembuh[i] = newSembuh[i] / max * 20;
    }

    final barGroup1 = makeGroupData(0, newSembuh[6], newPositif[6]);
    final barGroup2 = makeGroupData(1, newSembuh[5], newPositif[5]);
    final barGroup3 = makeGroupData(2, newSembuh[4], newPositif[4]);
    final barGroup4 = makeGroupData(3, newSembuh[3], newPositif[3]);
    final barGroup5 = makeGroupData(4, newSembuh[2], newPositif[2]);
    final barGroup6 = makeGroupData(5, newSembuh[1], newPositif[1]);
    final barGroup7 = makeGroupData(6, newSembuh[0], newPositif[0]);

    final items = [
      barGroup1,
      barGroup2,
      barGroup3,
      barGroup4,
      barGroup5,
      barGroup6,
      barGroup7,
    ];

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  covidIcon(),
                  const SizedBox(
                    width: 25,
                  ),
                  const Text(
                    'Data Covid-19 Indonesia',
                    style: TextStyle(
                        color: kTextColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  const Text(
                    '2021',
                    style: TextStyle(color: Color(0xff77839a), fontSize: 12),
                  ),
                ],
              ),
              const SizedBox(
                height: 38,
              ),
              Expanded(
                child: BarChart(
                  BarChartData(
                    maxY: 20,
                    barTouchData: BarTouchData(
                        touchTooltipData: BarTouchTooltipData(
                          tooltipBgColor: Colors.grey,
                          getTooltipItem: (_a, _b, _c, _d) => null,
                        ),
                        touchCallback: (FlTouchEvent event, response) {
                          if (response == null || response.spot == null) {
                            setState(() {
                              touchedGroupIndex = -1;
                              showingBarGroups = List.of(rawBarGroups);
                            });
                            return;
                          }

                          touchedGroupIndex =
                              response.spot!.touchedBarGroupIndex;

                          setState(() {
                            if (!event.isInterestedForInteractions) {
                              touchedGroupIndex = -1;
                              showingBarGroups = List.of(rawBarGroups);
                              return;
                            }
                            showingBarGroups = List.of(rawBarGroups);
                            if (touchedGroupIndex != -1) {
                              var sum = 0.0;
                              for (var rod
                                  in showingBarGroups[touchedGroupIndex]
                                      .barRods) {
                                sum += rod.y;
                              }
                              final avg = sum /
                                  showingBarGroups[touchedGroupIndex]
                                      .barRods
                                      .length;

                              showingBarGroups[touchedGroupIndex] =
                                  showingBarGroups[touchedGroupIndex].copyWith(
                                barRods: showingBarGroups[touchedGroupIndex]
                                    .barRods
                                    .map((rod) {
                                  return rod.copyWith(y: avg);
                                }).toList(),
                              );
                            }
                          });
                        }),
                    titlesData: FlTitlesData(
                      show: true,
                      rightTitles: SideTitles(showTitles: false),
                      topTitles: SideTitles(showTitles: false),
                      bottomTitles: SideTitles(
                        showTitles: true,
                        getTextStyles: (context, value) => const TextStyle(
                            color: Color(0xff7589a2),
                            fontWeight: FontWeight.bold,
                            fontSize: 14),
                        margin: 20,
                        getTitles: (double value) {
                          switch (value.toInt()) {
                            case 0:
                              return Env.dataCovid2.hari[6];
                            case 1:
                              return Env.dataCovid2.hari[5];
                            case 2:
                              return Env.dataCovid2.hari[4];
                            case 3:
                              return Env.dataCovid2.hari[3];
                            case 4:
                              return Env.dataCovid2.hari[2];
                            case 5:
                              return Env.dataCovid2.hari[1];
                            case 6:
                              return Env.dataCovid2.hari[0];
                            default:
                              return '';
                          }
                        },
                      ),
                      leftTitles: SideTitles(
                        showTitles: true,
                        getTextStyles: (context, value) => const TextStyle(
                            color: Color(0xff7589a2),
                            fontWeight: FontWeight.bold,
                            fontSize: 14),
                        margin: 8,
                        reservedSize: 28,
                        interval: 1,
                        getTitles: (value) {
                          if (value == 0) {
                            return thirdMax.toString();
                          } else if (value == 10) {
                            return secondMax.toString();
                          } else if (value == 19) {
                            return max.ceilToDouble().toString();
                          } else {
                            return '';
                          }
                        },
                      ),
                    ),
                    borderData: FlBorderData(
                      show: false,
                    ),
                    barGroups: showingBarGroups,
                    gridData: FlGridData(show: false),
                  ),
                ),
              ),
              SizedBox(
                  height: 44,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
                    child: Column(
                      children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                            width: 25,
                            height: 8,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    colors: [leftBarColor, leftBarColor]),
                                border: Border.all(
                                  color: leftBarColor,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                            child: Text('Sembuh'),
                          ),
                        ]),
                        Row(children: <Widget>[
                          Container(
                            width: 25,
                            height: 8,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    colors: [rightBarColor, rightBarColor]),
                                border: Border.all(
                                  color: rightBarColor,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                            child: Text('Positif'),
                          ),
                        ]),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1,
        colors: [leftBarColor],
        width: width,
      ),
      BarChartRodData(
        y: y2,
        colors: [rightBarColor],
        width: width,
      ),
    ]);
  }

  Widget covidIcon() {
    const width = 4.5;
    const space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: kTextMediumColor.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: kTextMediumColor.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: kTextMediumColor.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: kTextMediumColor.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: kTextMediumColor.withOpacity(0.4),
        ),
      ],
    );
  }
}
