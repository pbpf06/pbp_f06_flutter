import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'main.dart';

// get user data (name, email)
Future<Map<dynamic, dynamic>> getUserData() async {
  final prefs = await SharedPreferences.getInstance();
  int? userId = prefs.getInt('user_id');
  userId ??= 1;
  final response =
      await http.get(Uri.parse("${Env.URL_PREFIX}/getData/$userId"));
  var res = jsonDecode(response.body);
  return res;
}

// Remove use session when logout is clicked
void removeSession() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString('session_id', "");
  Env.isLoggedIn = false;
}

// save session on login/register
void setSession(int userId, String sessionId) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setInt('user_id', userId);
  prefs.setString('session_id', sessionId);
}

// Check if user session exists
Future<bool> sessionExists() async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.getInt('user_id') != null &&
      prefs.getString('session_id') != null) {
    return true;
  }
  return false;
}
Future<int?> getUserId() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getInt('user_id');
}

// authenticate user if session exists
Future<bool> is_authenticated() async {
  if (await sessionExists() == true) {
    final prefs = await SharedPreferences.getInstance();
    var userId = prefs.getInt('user_id');
    var sessionId = prefs.getString('session_id');
    final response = await http.post(Uri.parse("${Env.URL_PREFIX}/auth/"),
        headers: <String, String>{
          'Content-Type': 'application/json;charset=UTF-8'
        },
        body: jsonEncode(<String, dynamic>{
          'user_id': userId,
          'session_id': sessionId,
        }));
    var res = jsonDecode(response.body);
    if (res['is_authenticated'] == true) {
      return true;
    }
    return false;
  }
  return false;
}
