import 'package:pbp_f06/screens/model_wilayah.dart';
import 'package:pbp_f06/screens/register.dart';
import 'package:pbp_f06/fetch_data.dart';
import 'package:pbp_f06/screens/model_rs.dart';
import 'package:pbp_f06/screens/repository_rs.dart';
import 'package:pbp_f06/screens/repository_wilayah.dart';
import './constants.dart';
import 'screens/screens.dart';
import 'package:flutter/material.dart';
import 'utils.dart';
import 'fetch_data.dart';
import 'package:pbp_f06/classes/data_covid.dart';
import 'package:pbp_f06/classes/data_covid2.dart';

class Env {
  static String URL_PREFIX = "http://127.0.0.1:8000";
  static bool isLoggedIn = false;
  static DataCovid dataCovid =
      new DataCovid(positif: "0", dirawat: "0", sembuh: "0", meninggal: "0");
  static DataCovid2 dataCovid2 =
      new DataCovid2(positif: [], sembuh: [], hari: []);
  static List<Hospitals> hospitals = [];
  static List<Provinces> provinces = [];
  static List<Cities> cities = [];
  static List<Kecamatan> kecamatan = [];
  static List<Provinsi> provinsi = [];
  static List<Kota> kota = [];
  static Map<dynamic, dynamic> userData = {
    "name": "",
    "email": "",
    "first_name": "",
    "last_name": "",
    "gender": "",
    "date_of_birth": "",
    "number_phone": "",
    "province": "",
    "city": "",
    "district": "",
    "bio": ""
  };
}

Future<void> main() async {
  var data = {'url': 'https://data.covid19.go.id/public/api/update.json'};
  await RepositoryHospitals.getHospitals("11prop", "1171")
      .then((result) => Env.hospitals = result);
  await RepositoryProvinces.getProvinces()
      .then((result) => Env.provinces = result);
  await RepositoryCities.getCities("11prop")
      .then((result) => Env.cities = result);
  await RepositoryKecamatan.getKecamatan(1101)
      .then((result) => Env.kecamatan = result);
  await RepositoryProvinsi.getProvinsi()
      .then((result) => Env.provinsi = result);
  await RepositoryKota.getKota(11).then((result) => Env.kota = result);
  await fetchDataCovid(data).then((result) => Env.dataCovid = result);
  await fetchDataCovid2(data).then((result) => Env.dataCovid2 = result);
  await is_authenticated().then((result) => Env.isLoggedIn = result);
  await getUserData().then((result) {
    Env.userData['name'] = result['username'];
    Env.userData['email'] = result['email'];
    Env.userData['first_name'] = result['first_name'];
    Env.userData['last_name'] = result['last_name'];
    Env.userData['gender'] = result['gender'];
    Env.userData['date_of_birth'] = result['date_of_birth'];
    Env.userData['number_phone'] = result['number_phone'];
    Env.userData['province'] = result['province'];
    Env.userData['city'] = result['city'];
    Env.userData['district'] = result['district'];
    Env.userData['bio'] = result['bio'];
  });
  print(Env.dataCovid2);
  runApp(RestartWidget(
    child: MyApp(),
  ));
}

class RestartWidget extends StatefulWidget {
  RestartWidget({required this.child});

  final Widget child;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>()?.restartApp();
  }

  @override
  _RestartWidgetState createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // print(covidData.sembuh);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PBP-06 Covid-19',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        primarySwatch: Colors.green,
        scaffoldBackgroundColor: kBackgroundColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: Theme.of(context).textTheme.apply(displayColor: kTextColor),
      ),
      home: Env.isLoggedIn == true ? HomeScreen() : SignupPage(),
    );
  }
}
