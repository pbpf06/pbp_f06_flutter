import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/screens/model_wilayah.dart';

class RepositoryProvinsi {
  static const _provincesUrl =
      'https://dev.farizdotid.com/api/daerahindonesia/provinsi';

  static Future getProvinsi() async {
    try {
      final responseProvinces = await http.get(Uri.parse(_provincesUrl));
      if (responseProvinces.statusCode == 200) {
        Map<String, dynamic> province = jsonDecode(responseProvinces.body);
        Iterable itProvinces = province['provinsi'];
        List<Provinsi> provinsi =
            itProvinces.map((e) => Provinsi.fromJson(e)).toList();
        return provinsi;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}

class RepositoryKota {
  static const _citiesUrl =
      'https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=';

  static Future getKota(value) async {
    try {
      final responseCities = await http.get(Uri.parse("${_citiesUrl}${value}"));
      if (responseCities.statusCode == 200) {
        Map<String, dynamic> city = jsonDecode(responseCities.body);
        Iterable itCities = city['kota_kabupaten'];
        List<Kota> kota = itCities.map((e) => Kota.fromJson(e)).toList();
        return kota;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}

class RepositoryKecamatan {
  static const _districtUrl =
      'https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=';
  static Future getKecamatan(value) async {
    try {
      final responseDistricts = await http.get(Uri.parse("${_districtUrl}${value}"));
      if (responseDistricts.statusCode == 200) {
        Map<String, dynamic> district = jsonDecode(responseDistricts.body);
        Iterable itDistrict = district['kecamatan'];
        List<Kecamatan> kecamatan =
            itDistrict.map((e) => Kecamatan.fromJson(e)).toList();
        return kecamatan;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
