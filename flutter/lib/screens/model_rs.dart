class Provinces {
  final String idProv;
  final String name;

  Provinces({
    required this.idProv,
    required this.name,
  });

  factory Provinces.fromJson(Map<String, dynamic> json) {
    return Provinces(
      idProv: json['id'],
      name: json['name'],
    );
  }
}

class Cities {
  final String idCity;
  final String name;

  Cities({
    required this.idCity,
    required this.name,
  });

  factory Cities.fromJson(Map<String, dynamic> json) {
    return Cities(
      idCity: json['id'],
      name: json['name'],
    );
  }
}

class Hospitals {
  final String id;
  final String name;
  final String address;
  final String phone;
  final String info;

  Hospitals({
    required this.id,
    required this.name,
    required this.address,
    required this.phone,
    required this.info,
  });

  factory Hospitals.fromJson(Map<String, dynamic> json) {
    return Hospitals(
      id: json['id'],
      name: json['name'],
      address: json['address'],
      phone: json['phone'],
      info: json['info'],
    );
  }
}
