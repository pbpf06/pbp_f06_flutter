class Question {
  final int id, answer;
  final String question;
  final List<String> options;

  Question({required this.id, required this.question, required this.answer, required this.options});
}

const List sample_data = [
  {
    "id": 1,
    "question":
        "Apa nama ilmiah dari virus corona?",
    "options": ['COVID-19', 'SARS-CoV-2', 'Novel Coronavirus', 'Coronavirus'],
    "answer_index": 1,
  },
  {
    "id": 2,
    "question": "Apa kepanjangan dari ODP?",
    "options": ['Orang Dalam Pemantauan','Orang Dalam Penyakit', 'Orang Dalam Pengawasan', 'Orang Dalam Perlindungan' ],
    "answer_index": 0,
  },
  {
    "id": 3,
    "question": "Menjaga jarak aman secara fisik disebut juga?",
    "options": ['Social distancing', 'Karantina', 'Long distance', 'Physical distancing'],
    "answer_index": 3,
  },
  {
    "id": 4,
    "question": "Apa istilah mewabahnya suatu virus?",
    "options": ['Epidemik', 'Endemik', 'Pandemik', 'Epidermik'],
    "answer_index": 2,
  },
  {
    "id": 5,
    "question": "Lockdown dalam bahasa Indonesia disebut dengan?",
    "options": ['Karantina wilayah', 'Penutupan', 'Penguncian', 'Isolasi'],
    "answer_index": 0,
  },
];