import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:pbp_f06/main.dart' show Env;
import 'package:http/http.dart' as http;
import 'dart:convert';

class Berita extends StatefulWidget {
  Berita({ Key? key }) : super(key: key);

  @override
  _BeritaState createState() => _BeritaState();
}

class _BeritaState extends State<Berita> {

  int page_num=1;
  int showing = 10;
  String ordering = "num_liked";
  bool lock = false;
  List<Widget>? mywidget;
  bool disableButton = true;
  late int numOfElement = 0;

  static const Map<String, String> orderingMap = {
    "Paling dilihat": "viewed",
    "Terbaru":"created",
    "Paling banyak dikomentari": "num_commented",
    "Paling disuka": "num_liked"
  };

  static const Map<String, int> showingMap = {
    "10":10,
    "20":20,
    "30":30,
  };

  void getData() async
  {
    if (lock)
    {
      return;
    }
    lock = true;//acquire lock, unable to get request for a while
    var response = await http.get(Uri.parse("${Env.URL_PREFIX}/berita/?page_num=${page_num}&showing=${showing}&ordering=${ordering}"));
    if (response.statusCode==200)
    {
      Map<String, dynamic> map = jsonDecode(response.body);
      List<Widget> result = <Widget>[];
      numOfElement=map["num_of_element"];
      for (dynamic data in map["data"])
      {
        result.add(generateCardBerita(data));
      }
      setState(() {
        disableButton = false;
        mywidget = result;
      });
    }
    else
    {
      List<Widget> result = [CircularProgressIndicator()];
      setState((){
        disableButton=false;
        mywidget = result;
      });
    }
    lock = false;//unlock the lock
  }


  @override
  Widget build(BuildContext context) {
    if (mywidget == null)
    {
      getData();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Berita", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
              Container(
                margin: EdgeInsets.only(bottom:20),
               decoration: BoxDecoration(
                    border: Border.all(width: 3, color: Colors.green),
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.green,
                  ),
                child: Column(
                  children: <Widget>[
                    Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          Text("Urutkan berdasarkan", style: TextStyle(fontSize: 16,)),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            padding: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              border: Border.all(width: 2, color: Colors.blue),
                              borderRadius: BorderRadius.circular(5),
                            ),
                          child: DropdownButtonHideUnderline(
                             child: DropdownButton <String>(
                              icon: Icon(Icons.arrow_downward_rounded),
                              value: ordering,
                              items: orderingMap.keys.map<DropdownMenuItem<String>>((String txt) =>
                                DropdownMenuItem<String>(
                                  child: Text(txt, style: TextStyle(fontSize: 16,)),
                                  value: orderingMap[txt],
                                ) 
                                ).toList(),
                              onChanged: disableButton?null : (String? val){
                                if (val != null)
                                {
                                  setState((){
                                  ordering = val;
                                  disableButton=true;
                                  mywidget=null;
                                  page_num = 1;
                                  });
                                }
                              }
                              ),
                              )
                          )],
                      ),
                      Row(
                        children: [
                          Text("Jumlah ditampilkan", style: TextStyle(fontSize: 16,)),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            padding: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              border: Border.all(width: 2, color: Colors.blue),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: DropdownButtonHideUnderline(
                                   child: DropdownButton<int>(
                                      icon: Icon(Icons.arrow_downward_rounded),
                                      value: showing,
                                      items: showingMap.keys.map<DropdownMenuItem<int>>((String txt) =>
                                        DropdownMenuItem<int>(
                                          child: Text(txt, style: TextStyle(fontSize: 16,)),
                                          value: showingMap[txt],
                                        )
                                      ).toList()
                                      ,onChanged: disableButton?null: (int? val){
                                        if (val !=null)
                                        {
                                          setState((){
                                          showing = val;
                                          disableButton = true;
                                          mywidget = null;
                                          page_num=1;
                                          });
                                        }
                                      }
                                      ))
                              ,)
                        ],
                      )
                    ],
                )
                  ]+(mywidget ?? [SizedBox(width: double.infinity, height:500, child: Center(child: CircularProgressIndicator(),))])+
                  [
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10), 
                       child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: (disableButton || page_num==1)?null: (){
                                setState((){
                                  page_num--;
                                  disableButton=true;
                                  mywidget=null;
                                });
                              },
                              child: Icon(Icons.arrow_back)
                            ),
                            ElevatedButton(onPressed: null, child: Text("$page_num")),
                            ElevatedButton(
                              onPressed: (disableButton || page_num==(numOfElement/showing).ceil())?null: ()
                              {
                                setState((){
                                  page_num++;
                                  disableButton=true;
                                  mywidget=null;
                                });
                              }
                            , child: Icon(Icons.arrow_forward))
                          ],
                        ),
                      )),
                  ],
                ),

              ),
              
              BeritaRekomendasi(),
            ],
          ),
        )
      );
  }

  Widget generateCardBerita(Map<String, dynamic> map)
  {
    return Card(  
    child: Column(  
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[  
        ListTile( 
          leading: Icon(Icons.article_rounded, size:50, color: Colors.black),
          title: Text(map["title"]!),  
          subtitle: Text(map["date"]! +"\n"+map["author"]!),
          isThreeLine: true,
          onTap:() {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => BeritaView(author: map["author"]!, title:map["title"], id:map["id"])
            ));
          },
        ),  
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(child: Icon(Icons.favorite, color: Colors.red)),
                    TextSpan(text: "${map["liked"]!}"),
                  ]))),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(child: Icon(Icons.message, color: Colors.greenAccent)),
                    TextSpan(text: "${map["comments"]!}"),
                  ])
              )),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(child: Icon(Icons.visibility, color:Colors.grey)),
                      TextSpan(text: "${map["viewed"]!}"),
                    ])
                )),
            ]
          ,)
      ],  
    ),  
  );
  }
}