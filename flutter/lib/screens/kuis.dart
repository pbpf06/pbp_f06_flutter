import 'package:flutter/material.dart';
import 'package:pbp_f06/screens/quiz/quiz_screen.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import 'screens.dart';

class Kuis extends  StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*drawer: Container(
        width: MediaQuery.of(context).size.width * 0.74,
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
          child: NavigationDrawerWidget(),
        ),
      ),*/
      appBar: AppBar(
        title: Text("Kuis", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child:
        Container(
          alignment: Alignment.center,
          child: 
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.4),
            decoration: Style().buttonBoxDecoration(context),
            child: ElevatedButton(
              style: Style().buttonStyle(),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                child: Text(
                  "Mulai Kuis",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
              ),
              onPressed: () {
                showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30.0))),
                    scrollable: true,
                    title: Center(child: Text('Mulai Kuis?',
                    style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor)),),
                    content: Text("Apakah anda yakin ingin memulai kuis?"),
                    actions: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [ 
                          ElevatedButton(
                            child: Text("Tidak"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed))
                                    return Colors.red.shade100;
                                  return Colors.red;
                                  },
                                ),
                              ),
                            ),
                          SizedBox(width: 20,),
                          ElevatedButton(
                            child: Text("Mulai"),
                            onPressed: () {
                              Navigator.push(
                                context, MaterialPageRoute(
                                    builder: (context) => QuizScreen()
                                ),
                              );
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed))
                                    return kTextLightColor;
                                  return kPrimaryColor2;
                                  },
                                ),
                              ),
                            ),
                          ], 
                        ),
                      ],
                    );
                  }
                );
              },
            ),
          ), 
        ) 
      ,),
    );
  }
}