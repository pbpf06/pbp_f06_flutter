import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:pbp_f06/main.dart' show Env;
import 'dart:convert';
import '../widgets/widgets.dart';
import 'package:pbp_f06/utils.dart' show getUserId;
import 'package:pbp_f06/constants.dart';
import 'screens.dart';

class BeritaView extends StatefulWidget {
  final String title;
  final String author;
  final int id;
  BeritaView({ Key? key, required this.title, required this.author, required this.id }) : super(key: key);

  @override
  _BeritaViewState createState() => _BeritaViewState();
}

class _BeritaViewState extends State<BeritaView> {

  bool liked=false;
  int? likeNum;
  Widget? text;
  bool lock=false;
  int? viewed;
  int? userId;
  String? created;
  bool commentVisibility = false;
  List<Widget>? comments;
  final formKey = TextEditingController(text: "");
  Map<String, String> formValue={};

  Widget generateCommentCard(Map<String, dynamic> map)
  {
    return Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                  title: Text(map["author"]),
                  subtitle: Text(map["text"], overflow: TextOverflow.ellipsis),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(map["date"]),
                ),
              ]
            ),
          );
  }

  void getComments() async{
    if (comments!=null || lock)
    {
      return;
    }
    lock = true;//acquire lock, unable to get request for a while
    var response = await http.get(Uri.parse("${Env.URL_PREFIX}/berita/comment?id=${widget.id}"));

    if (response.statusCode==200)
    {
      Map<String, dynamic> data = jsonDecode(response.body);
      List<Widget> result = [];

      for (Map<String, dynamic> comment in data["data"])
      {
        result.add(generateCommentCard(comment));
      }
      setState((){
        comments = result;
      });
    }
    else
    {
      setState((){
        comments = <Widget>[Text("An error occurred")];
      });
    }
    lock = false;
  }

  void getText() async
  {
    if (lock)
    {
      return;
    }
    lock = true;
    userId = await getUserId();
    String link="";
    if (userId!=null)
    {
      link = "${Env.URL_PREFIX}/berita/view?id=${widget.id}&user-id=${userId}";
    }
    else
    {
      link = "${Env.URL_PREFIX}/berita/view?id=${widget.id}";
    }
    var request = await http.get(Uri.parse(link));
    if (request.statusCode==200)
    {
      Map<String, dynamic> map = jsonDecode(request.body);
      setState((){
        liked = map["liked"];
        created=map["date"];
        viewed = map["viewed"];
        likeNum = map["like_num"];
        text = Text(map["text"],
        textAlign: TextAlign.justify);
      });
    }
    else
    {
        setState((){
          text = Text("An error occurred");
        });
        
    }
    lock = false;
  }


  @override
  Widget build(BuildContext context) {
    if (text == null)
    {
      getText();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title,
          style: TextStyle(fontWeight: FontWeight.w600,
            color: kPrimaryColor,
            ),
          ),
          elevation: 0,
          iconTheme: IconThemeData(color: kPrimaryColor),
          backgroundColor: kBackgroundColor,
        ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 30),
              decoration: BoxDecoration(
                border: Border.all(width:3, color: Colors.green),
                borderRadius: BorderRadius.circular(5),
                color: Colors.green,
              ),
              child: Column(
                children:[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("By ${widget.author}"),
                    Text(created ?? "N/A")
                  ],
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  constraints: BoxConstraints(
                    minHeight: 400,
                    minWidth: double.infinity,
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 2),
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                  ),

                  child: text??(SizedBox(width: double.infinity, height:500, child: Center(child: CircularProgressIndicator(),))),
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ElevatedButton(
                        onPressed: (text==null)?null:((userId==null || userId!<1)?(
                        ){//kasus belum login
                          showDialog<String>(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text("Login required"),
                              content: const Text("Kamu harus login terlebih dahulu sebelum komen"),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context, 'Cancel'),
                                  child: const Text("Batalkan"),
                                )
                              ],
                            )
                          );
                        }:()  {//kasus udah login

                            setState(() {
                              commentVisibility= !commentVisibility;
                            });
                            getComments();
                        }),
                        child: const Text("Comment")),
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.lightGreen[100],
                          ),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child:Icon(Icons.favorite, color: (liked)?Colors.red:Colors.grey),
                                ),
                                TextSpan(
                                  text: "${likeNum??"N\A"}",
                                ),
                              ],
                            )
                          ),
                          onPressed: (text==null)?null: () async {
                            if (userId==null || userId!<1)
                            {
                                showDialog<String>(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    title: const Text("Login required"),
                                    content: const Text("Kamu harus login terlebih dahulu sebelum komen"),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () => Navigator.pop(context, 'Cancel'),
                                        child: const Text("Batalkan"),
                                      )
                                    ],
                                  )
                                );
                            }
                            else
                            {
                              if (liked)
                              {
                                likeNum = likeNum!-1;
                              }
                              else
                              {
                                likeNum = likeNum!+1;
                              }
                                setState((){
                                  liked = !liked;
                                });
                              http.post(Uri.parse("${Env.URL_PREFIX}/berita/like?id=${widget.id}&user-id=${userId}&liked=${liked?1:0}"));
                            }
                          },)
                    ],
                  ),
                  Visibility(
                    visible: commentVisibility,
                    child: Container(
                        child: Column(
                          children: (comments??[(SizedBox(width: double.infinity, height:500, child: Center(child: CircularProgressIndicator(),)))])+
                          [
                            Container(
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.symmetric(vertical:0, horizontal: 9),
                              decoration: BoxDecoration(
                                border: Border.all(width:3),
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                            child: TextField(
                              controller: formKey,
                              decoration: null
                            )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  margin: EdgeInsets.all(20),
                                  child: ElevatedButton(
                                  onPressed: (){
                                    setState((){
                                      commentVisibility=false;
                                    });
                                  }, child: Text("Cancel"))),
                                  Container(
                                    margin: EdgeInsets.all(20),
                                    child: ElevatedButton(onPressed: () async{
                                        var response = await http.post(Uri.parse("${Env.URL_PREFIX}/berita/comment?id=${widget.id}"),
                                        body: <String, String>{
                                          "user-id":"${userId}",
                                          "text": formKey.text,
                                        }
                                      );
                                      if (response.statusCode==200)
                                      {
                                        setState((){
                                          formKey.text="";
                                          comments ??=<Widget>[];
                                          comments!.add(generateCommentCard(jsonDecode(response.body)));
                                        });
                                      }
                                      else
                                      {
                                        print("Error");
                                      }
                                    },
                                      child: Text("Submit")))
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                ]
              ),
            ),
            BeritaRekomendasi(),
          ],
        ),
      ),
      );
  }
}
