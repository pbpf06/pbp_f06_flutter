class Provinsi {
  final int idProv;
  final String nama;

  Provinsi({
    required this.idProv,
    required this.nama,
  });

  factory Provinsi.fromJson(Map<String, dynamic> json) {
    return Provinsi(
      idProv: json['id'],
      nama: json['nama'],
    );
  }
}

class Kota {
  final int idKota;
  final String nama;

  Kota({
    required this.idKota,
    required this.nama,
  });

  factory Kota.fromJson(Map<String, dynamic> json) {
    return Kota (
      idKota: json['id'],
      nama: json['nama'],
    );
  }
}

class Kecamatan {
  final int idKec;
  final String nama;

  Kecamatan({
    required this.idKec,
    required this.nama,
  });

  factory Kecamatan.fromJson(Map<String, dynamic> json) {
    return Kecamatan(
      idKec: json['id'],
      nama: json['nama'],
    );
  }
}
