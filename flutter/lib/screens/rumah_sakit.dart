import 'package:flutter/material.dart';
import 'package:pbp_f06/config/styles.dart';
import '../constants.dart';
import 'repository_rs.dart';
import '../main.dart';

class RumahSakit extends StatefulWidget {
  const RumahSakit({
    Key? key,
  }) : super(key: key);

  @override
  _RumahSakitState createState() => _RumahSakitState();
}

class _RumahSakitState extends State<RumahSakit> {
  var _dataHospitals = Env.hospitals;
  var _valHospitals;
  var _dataProvinces = Env.provinces;
  var _valProvinces;
  var _dataCities = Env.cities;
  var _valCities;
  Map<String, String> ids = {};
  Map<String, String> ids2 = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          iconTheme: IconThemeData(color: kPrimaryColor),
          backgroundColor: kBackgroundColor,
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.fromLTRB(25, 35, 25, 10),
            alignment: Alignment.center,
            child: Column(
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  //TITLE
                  Text("Daftar Rumah Sakit",
                  style: TextStyle(fontWeight: FontWeight.w900, color: kPrimaryColor, fontSize: 24)
                ),
                  SizedBox(height: 20,),
                  //ICON
                  const Icon(
                    Icons.local_hospital_rounded,
                    color: kPrimaryColor,
                    size: 100,
                  ),

                  //NEW LINE
                  SizedBox(height: 30,),

                  //DROPDOWN PROVINSI
                  SizedBox(
                    width: 400,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor2)),
                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor)),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(100.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                        ),
                        dropdownColor: kBackgroundColor,
                        hint: const Text("Pilih Provinsi"),
                        value: _valProvinces,
                        items: _dataProvinces.map((item) {
                          ids[item.name] = item.idProv;
                          return DropdownMenuItem(
                            child: Text(item.name),
                            value: item.name,
                          );
                        }).toList(),
                        onChanged: (value) async {
                          var tempData =
                              await RepositoryCities.getCities(ids[value]);
                          setState(() {
                            _valProvinces = value;
                            _dataCities = tempData;
                          });
                        },
                      ),
                    ),
                  ),

                  SizedBox(height: 20,),

                  //DROPDOWN KABUPATEN/KOTA
                  SizedBox(
                    width: 400,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor2)),
                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor)),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(100.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                        ),
                        dropdownColor: kBackgroundColor,
                        hint: const Text("Pilih Kabupaten/Kota"),
                        value: _valCities,
                        items: _dataCities.map((item) {
                          ids2[item.name] = item.idCity;
                          return DropdownMenuItem(
                            child: Text(item.name),
                            value: item.name,
                          );
                        }).toList(),
                        onChanged: (value) async {
                          setState(() {
                            _valCities = value;
                          });
                          // print(ids[_valProvinces]);
                          // print(ids2[_valCities]);
                          var tempData = await RepositoryHospitals.getHospitals(
                              ids[_valProvinces], ids2[_valCities]);
                          setState(() {
                            _dataHospitals = tempData;
                          });
                        },
                      ),
                    ),
                  ),

                  SizedBox(height: 20,),    

                  //DROPDOWN RS
                  SizedBox(
                    width: 400,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: DropdownButtonFormField(
                        isExpanded: true,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor2)),
                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: kPrimaryColor)),
                          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(100.0), borderSide: BorderSide(color: Colors.red, width: 0.5)),
                        ),
                        dropdownColor: kBackgroundColor,
                        hint: const Text("Rumah Sakit"),
                        value: _valHospitals,
                        items: _dataHospitals.map((item) {
                          return DropdownMenuItem(
                            child: Text(
                              item.name +
                                  "\n" +
                                  item.address +
                                  "\n" +
                                  item.phone +
                                  "\n" +
                                  item.info +
                                  "\n",
                            ),
                            value: item.name,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _valHospitals = value;
                          });
                        },
                      ),
                    ),
                  ),

                  SizedBox(height: 30,),

                  // ignore: deprecated_member_use
                  Container(
                    decoration: Style().buttonBoxDecoration(context),
                    child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Cek",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                    BorderRadius.all(Radius.circular(30.0))),
                                title: Center(child: Text("Hasil pengecekan.",
                                    style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                content: Text(
                                    "Rumah sakit tersedia.", textAlign: TextAlign.center,),
                                actions: [
                                  Center(
                                    child: ElevatedButton(
                                      child: Text("Tutup"),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.resolveWith<Color>(
                                          (Set<MaterialState> states) {
                                            if (states.contains(MaterialState.pressed))
                                              return kTextLightColor;
                                            return kPrimaryColor2;
                                          },
                                        ),
                                      ),
                                    )
                                  )
                                ]
                              );
                            },
                          );
                        }
                      ),
                  ),
                ]),
          ),
        ));
  }
}
