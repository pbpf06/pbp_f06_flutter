import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/main.dart';
import 'dart:convert';

class ForgotPassword extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _ForgotPassword();
  }
  static String kode = '';
  static String uname = '';
}

// cek user into app
Future<String> cekuname(String username) async {
  final response = await http.post(Uri.parse("${Env.URL_PREFIX}/reset_password/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(<String, dynamic>{
        'username': username,
      }));
  var res = jsonDecode(response.body);
  String result ='';
  if (res['status'] == 'sent') {
    result = res['rotp'].toString();
    return result;
  }
  else if (res['status'] == 'failed'){
    return 'failed';
  }
  else{
    return 'error';
  }
  //return result;
  //int userId = res['user_id'];
  //String sessionId = res['session_id'];
}


class _ForgotPassword extends State<ForgotPassword>{

  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController kodeController = TextEditingController();
  bool checkedValue = false;
  bool checkboxValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        automaticallyImplyLeading: false,
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child:
          Container(
            margin: EdgeInsets.fromLTRB(30, MediaQuery.of(context).size.height * 0.2, 30, 10),
            child: Column(
              children: [
                Text("Reset Your Password",
                  style: TextStyle(fontWeight: FontWeight.w900, color: kPrimaryColor, fontSize: 24)
                ),
                SizedBox(height: 40.0),
               Form(
                    child: Column(children: [
                  Container(
                    child: TextFormField(
                      decoration: Style().textInputDecoration("Username", "", usernameController),
                      controller: usernameController,
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    decoration: Style().buttonBoxDecoration(context),
                    child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Selanjutnya",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () async {
                          String username = usernameController.text;
                          
                          if(username.compareTo('')!=0){
                            String result = await cekuname(username);
                            if(result == 'error'){
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                        BorderRadius.all(Radius.circular(30.0))),
                                    title: Center(child: Text("Error",
                                    style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                    content: Text(
                                        "Terjadi error", textAlign: TextAlign.center,),
                                    actions: [
                                      Center(
                                        child: ElevatedButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            usernameController.text = "";
                                            Navigator.of(context).pop();
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                            MaterialStateProperty.resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                              if (states.contains(MaterialState.pressed))
                                                return kTextLightColor;
                                              return kPrimaryColor2;
                                              },
                                            ),
                                          ),
                                        )
                                      )
                                    ]
                                  );
                                }
                            );
                          }
                            else if(result == 'failed'){
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                          BorderRadius.all(Radius.circular(30.0))),
                                      title: Center(child: Text("Gagal.",
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                      content: Text(
                                          "Username salah.", textAlign: TextAlign.center,),
                                      actions: [
                                        Center(
                                          child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              usernameController.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                  if (states.contains(MaterialState.pressed))
                                                    return kTextLightColor;
                                                  return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          )
                                        )
                                      ]
                                    );
                                  }
                              );
                            }
                            else{
                              ForgotPassword.kode = result;
                              ForgotPassword.uname = usernameController.text;
                              (Navigator.push(context, MaterialPageRoute( builder: (context) => VerifyCode()),));
                              //(Navigator.push(context, MaterialPageRoute( builder: (context) => NewPassword()),));
                            }
                          }
                          else{
                            showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                          BorderRadius.all(Radius.circular(30.0))),
                                      title: Center(child: Text("Gagal.",
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                      content: Text(
                                          "Masukkan username.", textAlign: TextAlign.center,),
                                      actions: [
                                        Center(
                                          child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              usernameController.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                  if (states.contains(MaterialState.pressed))
                                                    return kTextLightColor;
                                                  return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          )
                                        )
                                      ]
                                    );
                                  }
                              );
                          }
                          
                        },
                      ),
                    ),
                    FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginDemo()),
                      );
                    },
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                        child: Text(
                          "Login",
                          style: TextStyle(color: kPrimaryColor, fontSize: 15),
                        )),
                  ),
                  SizedBox(height: 30.0),
                    SizedBox(height: 30.0),
                  ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}