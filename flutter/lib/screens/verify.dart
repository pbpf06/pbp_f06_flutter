import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/main.dart';
import 'dart:convert';
import 'forgot_password.dart';

class VerifyCode extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _VerifyCode();
  }
}

// cek user into app
bool cekverify(String kode) {
  if (ForgotPassword.kode.compareTo(kode)==0) {
    return true;
  }
  else{
    return false;
  }
}

class _VerifyCode extends State<VerifyCode>{

  //final _formKey = GlobalKey<FormState>();
  TextEditingController kodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        automaticallyImplyLeading: false,
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child:
          Container(
            margin: EdgeInsets.fromLTRB(30, MediaQuery.of(context).size.height * 0.2, 30, 10),
            child: Column(
              children: [
                Text("Verifikasi Kode",
                  style: TextStyle(fontWeight: FontWeight.w900, color: kPrimaryColor, fontSize: 24)
                ),
                SizedBox(height: 40.0),
                Form(
                    child: Column(children: [
                  Container(
                    child: TextFormField(
                      decoration: Style().textInputDecoration("Kode", "****", kodeController),
                      controller: kodeController,
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Verify",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () {
                          String kode = kodeController.text;
                          bool result = cekverify(kode);
                          if(kode.isNotEmpty&&kode!=''){
                            if(result == true){
                            (Navigator.push(context, MaterialPageRoute( builder: (context) => NewPassword()),));
                          }
                            else{
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                          BorderRadius.all(Radius.circular(30.0))),
                                      title: Center(child: Text("Gagal",
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                      content: Text(
                                          "Kode yang Anda masukkan salah!", textAlign: TextAlign.center,),
                                      actions: [
                                        Center(
                                          child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              kodeController.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                              MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return kTextLightColor;
                                                return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          )
                                        )
                                      ]
                                    );
                                  }
                              );
                            }
                          }
                          else{
                            showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                          BorderRadius.all(Radius.circular(30.0))),
                                      title: Center(child: Text("Gagal",
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                      content: Text(
                                          "Masukkan kode!", textAlign: TextAlign.center,),
                                      actions: [
                                        Center(
                                          child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              kodeController.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                              MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return kTextLightColor;
                                                return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          )
                                        )
                                      ]
                                    );
                                  }
                              );
                          }
                          
                        },
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Kembali",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () {
                         (Navigator.push(context, MaterialPageRoute( builder: (context) => ForgotPassword()),));
                        },
                      ),
                    ),
                     SizedBox(height: 20.0),
                  ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}