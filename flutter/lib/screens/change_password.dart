import 'package:flutter/material.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../screens/screens.dart';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/main.dart';
import 'dart:convert';

import '../utils.dart';

class ChangePassword extends StatefulWidget{
  const ChangePassword({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
     return _ChangePassword();
  }
}

Future<String> changePassword(Map<String, String> data) async {
  final response = await http.post(Uri.parse("${Env.URL_PREFIX}/password/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));
  var res = jsonDecode(response.body);
  return res['msz'];
}

class _ChangePassword extends State<ChangePassword>{

  final _formKey = GlobalKey<FormState>();
  bool checkedValue = false;
  bool checkboxValue = false;
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController oldPassword1Controller = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*drawer: Container(
        width: MediaQuery.of(context).size.width * 0.74,
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
          child: NavigationDrawerWidget(),
        ),
      ),*/
      appBar: AppBar(
        title: Text("Password", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child:
        Container (
          margin: EdgeInsets.fromLTRB(25, 35, 25, 10),
          alignment: Alignment.center,
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Password Lama", "*********", oldPasswordController),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password lama anda";
                          }
                          else if (oldPassword1Controller.text != oldPasswordController.text) {
                            return "Password yang anda masukkan tidak sesuai";
                          }
                          return null;
                        },
                        controller: oldPasswordController,
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Ulangi Password Lama", "*********", oldPassword1Controller),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password lama anda";
                          }
                          else if (oldPassword1Controller.text != oldPasswordController.text) {
                            return "Password yang anda masukkan tidak sesuai";
                          }
                          return null;
                        },
                        controller: oldPassword1Controller,
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Password Baru", "*********", newPasswordController),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password baru anda";
                          }
                          return null;
                        },
                        controller: newPasswordController,
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 30.0),
                    Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Ganti Password",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () async {
                          final prefs =
                              await SharedPreferences.getInstance();
                          int? userId = prefs.getInt('user_id');
                          if (_formKey.currentState!.validate()) {
                            Map<String, String> data = {};
                            data['user_id'] = userId.toString();
                            data['cpwd'] = oldPasswordController.text;
                            data['npwd'] = newPasswordController.text;
                            String result = await changePassword(data);
                            result == "Berhasil mengganti password."
                            ? showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                        BorderRadius.all(Radius.circular(30.0))),
                                    title: Center(child: Text(result,
                                    style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                    content: Text(
                                        "Mohon ulangi login untuk memastikan password yang anda masukkan benar.", 
                                        textAlign: TextAlign.center,),
                                    actions: [
                                      Center(
                                        child: ElevatedButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            removeSession();
                                            Restart.restartApp();
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return kTextLightColor;
                                                return kPrimaryColor2;
                                              },
                                            ),
                                          ),
                                        )
                                      )
                                    ]
                                  );
                                },
                              )
                            : showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                        BorderRadius.all(Radius.circular(30.0))),
                                    title: Container(
                                      child: Text("Gagal mengganti password.", 
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor)),),
                                    content: Text(
                                        result, textAlign: TextAlign.center,),
                                    actions: [
                                      Center(
                                        child: ElevatedButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            Navigator.of(context)
                                                .pop();
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return kTextLightColor;
                                                return kPrimaryColor2;
                                              },
                                            ),
                                          ),
                                        )
                                      )
                                    ]
                                  );
                                },
                              );
                          }
                        },
                      ),
                    ),
                    SizedBox(height: 30.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}