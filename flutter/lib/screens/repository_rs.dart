import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/screens/model_rs.dart';

class RepositoryProvinces {
  static const _provincesUrl =
      'https://rs-bed-covid-api.vercel.app/api/get-provinces';

  static Future getProvinces() async {
    try {
      final responseProvinces = await http.get(Uri.parse(_provincesUrl));
      if (responseProvinces.statusCode == 200) {
        Map<String, dynamic> province = jsonDecode(responseProvinces.body);
        Iterable itProvinces = province['provinces'];
        List<Provinces> provinces =
            itProvinces.map((e) => Provinces.fromJson(e)).toList();
        return provinces;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}

class RepositoryCities {
  static const _citiesUrl =
      'https://rs-bed-covid-api.vercel.app/api/get-cities?provinceid=';

  static Future getCities(value) async {
    try {
      final responseCities = await http.get(Uri.parse("${_citiesUrl}${value}"));
      if (responseCities.statusCode == 200) {
        Map<String, dynamic> city = jsonDecode(responseCities.body);
        Iterable itCities = city['cities'];
        List<Cities> cities = itCities.map((e) => Cities.fromJson(e)).toList();
        return cities;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}

class RepositoryHospitals {
  static Future getHospitals(_valProvince, _valCity) async {
    try {
      final responseHospitals = await http.get(Uri.parse(
          'https://rs-bed-covid-api.vercel.app/api/get-hospitals?provinceid=${_valProvince}&cityid=${_valCity}&type=1'));
      if (responseHospitals.statusCode == 200) {
        Map<String, dynamic> hos = jsonDecode(responseHospitals.body);
        Iterable itHospitals = hos['hospitals'];
        List<Hospitals> hospitals =
            itHospitals.map((e) => Hospitals.fromJson(e)).toList();
        return hospitals;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
