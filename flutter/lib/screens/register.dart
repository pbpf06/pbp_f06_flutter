import 'package:flutter/material.dart';
import 'package:pbp_f06/screens/login.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:pbp_f06/main.dart';
import 'package:pbp_f06/utils.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

// register user into app
Future<bool> register(Map<String, String> data) async {
  final response = await http.post(Uri.parse("${Env.URL_PREFIX}/register/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));
  var res = jsonDecode(response.body);
  if (res['success'] == false) {
    return false;
  }
  int userId = res['user_id'];
  String sessionId = res['session_id'];
  setSession(userId, sessionId);
  return true;
}

class _SignupPageState extends State<SignupPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController password1Controller = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  bool _isHidden = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.fromLTRB(
                30, MediaQuery.of(context).size.height * 0.1, 30, 10),
            child: Column(
              children: [
                Text("Daftar Akun Baru!",
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        color: kPrimaryColor,
                        fontSize: 24)),
                SizedBox(height: 40.0),
                Form(
                    child: Column(children: [
                  Container(
                    child: TextFormField(
                      decoration: Style().textInputDecoration(
                          "Alamat Email", "", emailController),
                      controller: emailController,
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 30.0),
                  Container(
                    child: TextFormField(
                      decoration: Style().textInputDecoration(
                          "Username", "", usernameController),
                      controller: usernameController,
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 30.0),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      width: 1500,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(22),
                          border: Border.all(
                              width: 1,
                              color: kPrimaryColor,
                              style: BorderStyle.solid)),
                      child: TextFormField(
                        controller: password1Controller,
                        obscureText: _isHidden,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: "Kata Sandi",
                          contentPadding: EdgeInsets.all(15),
                          border: InputBorder.none,
                          suffix: GestureDetector(
                            onTap: () {
                              _togglePasswordView();
                            },
                            child: Icon(
                              _isHidden
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: _isHidden ? Colors.grey : Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 30.0),
                  Container(
                    alignment: Alignment.center,
                    child: Container(
                      width: 1500,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(22),
                          border: Border.all(
                              width: 1,
                              color: kPrimaryColor,
                              style: BorderStyle.solid)),
                      child: TextFormField(
                        controller: password2Controller,
                        obscureText: _isHidden,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: "Ulangi Kata Sandi",
                          contentPadding: EdgeInsets.all(15),
                          border: InputBorder.none,
                          suffix: GestureDetector(
                            onTap: () {
                              _togglePasswordView();
                            },
                            child: Icon(
                              _isHidden
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: _isHidden ? Colors.grey : Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 25.0),
                  Container(
                    decoration: Style().buttonBoxDecoration(context),
                    child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            "Daftarkan Akun",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () async {
                          Map<String, String> data = {};
                          data['username'] = usernameController.text;
                          data['email'] = emailController.text;
                          data['password1'] = password1Controller.text;
                          data['password2'] = password2Controller.text;
                          bool result = await register(data);
                          if (result == true) {
                            await getUserData().then((result) {
                              Env.userData['name'] = result['username'];
                              Env.userData['email'] = result['email'];
                              Env.userData['first_name'] = result['first_name'];
                              Env.userData['last_name'] = result['last_name'];
                              Env.userData['gender'] = result['gender'];
                              Env.userData['date_of_birth'] =
                                  result['date_of_birth'];
                              Env.userData['number_phone'] =
                                  result['number_phone'];
                              Env.userData['province'] = result['province'];
                              Env.userData['city'] = result['city'];
                              Env.userData['district'] = result['district'];
                              Env.userData['bio'] = result['bio'];
                            });
                          }
                          result == true
                              ? Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomeScreen()),
                                )
                              : showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(30.0))),
                                        title: Center(
                                            child: Text("Registrasi gagal.",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    color: kPrimaryColor))),
                                        content: Text(
                                          "Data registrasi tidak valid.",
                                          textAlign: TextAlign.center,
                                        ),
                                        actions: [
                                          Center(
                                              child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              emailController.text = "";
                                              usernameController.text = "";
                                              password1Controller.text = "";
                                              password2Controller.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty
                                                      .resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                  if (states.contains(
                                                      MaterialState.pressed))
                                                    return kTextLightColor;
                                                  return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          ))
                                        ]);
                                  },
                                );
                        }),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginDemo()),
                      );
                    },
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Text(
                          "Sudah Punya akun? Login disini",
                          style: TextStyle(color: kPrimaryColor, fontSize: 15),
                        )),
                  ),
                  SizedBox(height: 30.0),
                ])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}
