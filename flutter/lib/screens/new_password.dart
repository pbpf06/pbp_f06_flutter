import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';
import 'package:http/http.dart' as http;
import 'package:pbp_f06/main.dart';
import 'dart:convert';
import 'forgot_password.dart';

class NewPassword extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _NewPassword();
  }
}

// cek user into app
Future<bool> newpass(String username, String npass) async {
  final response = await http.post(Uri.parse("${Env.URL_PREFIX}/forgotpassword/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(<String, dynamic>{
        'username': username,
        'npass' : npass,
      }));
  var res = jsonDecode(response.body);
  if(res['success']==false){
    return false;
  }
  return true;
}


class _NewPassword extends State<NewPassword>{

  //final _formKey = GlobalKey<FormState>();
  TextEditingController newpassController = TextEditingController();
  TextEditingController newpass1Controller = TextEditingController();
  bool checkedValue = false;
  bool checkboxValue = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        automaticallyImplyLeading: false,
        backgroundColor: kBackgroundColor,
      ),
       body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child:
          Container(
            margin: EdgeInsets.fromLTRB(30, MediaQuery.of(context).size.height * 0.2, 30, 10),
            child: Column(
              children: [
                Text("Password Baru",
                  style: TextStyle(fontWeight: FontWeight.w900, color: kPrimaryColor, fontSize: 24)
                ),
                SizedBox(height: 40.0),
                Form(
                    child: Column(children: [
                  Container(
                    child: TextFormField(
                      obscureText: true,
                      decoration: Style().textInputDecoration("Password Baru", "*********", newpassController),
                      validator: (val) {
                          if (val!.isEmpty) {
                            return "Mohon masukkan password baru anda";
                          }
                          return null;
                      },
                      controller: newpassController,
                    ),
                    decoration: Style().inputBoxDecorationShaddow(),
                  ),
                  SizedBox(height: 30.0),
                  Container(
                      child: TextFormField(
                        obscureText: true,
                        decoration: Style().textInputDecoration(
                            "Ulangi Password Baru", "*********", newpass1Controller),
                          validator: (val) {
                          if (val!.isEmpty) {
                            return "Ulangi password baru anda";
                          }
                          else if (newpass1Controller.text != newpassController.text) {
                            return "Password yang anda masukkan tidak sesuai";
                          }
                          return null;
                        },
                        controller: newpass1Controller,
                      ),
                      decoration: Style().inputBoxDecorationShaddow(),
                    ),
                    SizedBox(height: 30.0),

                    Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Ganti Password",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () async {
                          String username = ForgotPassword.uname;
                          String npass = newpassController.text;
                          if(newpassController.text.isNotEmpty && newpassController.text!='' && newpassController.text==newpass1Controller.text){
                            bool result = await newpass(ForgotPassword.uname, npass);
                            if(result == true){
                              (Navigator.push(context, MaterialPageRoute( builder: (context) => LoginDemo()),));
                            }
                            else{
                            showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                          BorderRadius.all(Radius.circular(30.0))),
                                      title: Center(child: Text("Gagal",
                                      style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                      content: Text(
                                          "Tidak berhasil mengganti password!", textAlign: TextAlign.center,),
                                      actions: [
                                        Center(
                                          child: ElevatedButton(
                                            child: Text("OK"),
                                            onPressed: () {
                                              newpassController.text = "";
                                              Navigator.of(context).pop();
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                              MaterialStateProperty.resolveWith<Color>(
                                                (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return kTextLightColor;
                                                return kPrimaryColor2;
                                                },
                                              ),
                                            ),
                                          )
                                        )
                                      ]
                                    );
                                }
                                );
                              }
                            }
                            else{
                              showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                            BorderRadius.all(Radius.circular(30.0))),
                                        title: Center(child: Text("Gagal",
                                        style: TextStyle(fontWeight: FontWeight.w500, color: kPrimaryColor))),
                                        content: Text(
                                            "Masukkan password baru dengan benar dan tepat!", textAlign: TextAlign.center,),
                                        actions: [
                                          Center(
                                            child: ElevatedButton(
                                              child: Text("OK"),
                                              onPressed: () {
                                                newpassController.text = "";
                                                Navigator.of(context).pop();
                                              },
                                              style: ButtonStyle(
                                                backgroundColor:
                                                MaterialStateProperty.resolveWith<Color>(
                                                  (Set<MaterialState> states) {
                                                  if (states.contains(MaterialState.pressed))
                                                    return kTextLightColor;
                                                  return kPrimaryColor2;
                                                  },
                                                ),
                                              ),
                                            )
                                          )
                                        ]
                                      );
                                  }
                                  );
                            }
                          
                        },
                      ),
                    ),
                    SizedBox(height: 30.0),
                     Container(
                      decoration: Style().buttonBoxDecoration(context),
                      child: ElevatedButton(
                        style: Style().buttonStyle(),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                          child: Text(
                            "Kembali",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        onPressed: () {
                         (Navigator.push(context, MaterialPageRoute( builder: (context) => VerifyCode()),));
                        },
                      ),
                    ),
                     SizedBox(height: 20.0),
                 ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}