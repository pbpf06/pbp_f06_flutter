import 'package:flutter/material.dart';
import '../config/styles.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../screens/screens.dart';

class Forum extends  StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*drawer: Container(
        width: MediaQuery.of(context).size.width * 0.74,
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
          child: NavigationDrawerWidget(),
        ),
      ),*/
      appBar: AppBar(
        title: Text("Forum", 
          style: TextStyle(fontWeight: FontWeight.w600,
          color: kPrimaryColor
          ),
        ),
        elevation: 0,
        iconTheme: IconThemeData(color: kPrimaryColor),
        backgroundColor: kBackgroundColor,
      ),
    );
  }
}