import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:pbp_f06/classes/data_covid.dart';
import 'package:pbp_f06/classes/data_covid2.dart';

// fetch covid data
Future<DataCovid> fetchDataCovid(Map<String, dynamic> data) async {
  final response = await http.post(
      Uri.parse("http://127.0.0.1:8000/fetchData/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    var res = jsonDecode(response.body);
    res = res['update']['total'];
    // int sembuh = res['update']['total']['jumlah_sembuh'];
    return DataCovid.fromJson(res);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load data.');
  }
}

Future<DataCovid2> fetchDataCovid2(Map<String, dynamic> data) async {
  final response = await http.post(
      Uri.parse("http://127.0.0.1:8000/fetchData/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    var res = jsonDecode(response.body);
    List<dynamic> data = res['update']['harian'];
    int index = data.length - 1;
    Map<String, List<String>> newData = {
      "positif": [],
      "sembuh": [],
      "hari": [],
    };
    int n = 0;
    while (n < 8) {
      newData['positif']
          ?.add(data[index]['jumlah_positif']['value'].toString());
      newData['sembuh']?.add(data[index]['jumlah_sembuh']['value'].toString());
      newData['hari']
          ?.add(data[index]['key_as_string'].toString().substring(5, 10));
      n++;
      index--;
    }
    print(newData);

    return DataCovid2.fromJson(newData);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load data.');
  }
}
