from django.urls import path
from .views import forgotpassword, reset_password

urlpatterns = [
    path('reset_password/', reset_password, name='reset_password'),
    path('forgotpassword/', forgotpassword, name='forgotpassword'),
]