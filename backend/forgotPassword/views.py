from django.http.response import JsonResponse
from django.core.mail import EmailMessage
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
import random
import json
from django.views.decorators.csrf import csrf_exempt
from user.models import Profile
from smtplib import SMTPException

# Create your views here.
@csrf_exempt
def forgotpassword(request):
    data = {"status": "New password gagal.", "success": False}
    if request.method == "POST":
        user_data = json.loads(request.body)
        un = user_data["username"]
        pwd = user_data["npass"]
        user = User.objects.get(username=un)
        user.set_password(pwd)
        user.save()

        data = {"status": "New password berhasil.", "success": True}

    return JsonResponse(data)
    
@csrf_exempt
def reset_password(request):
    if request.method == "POST":
        user_data = json.loads(request.body)
        username = user_data['username']

        
        #user = User.objects.get(username=username)
        try:
            user = get_object_or_404(User, username=username)
        #if user is not None:
            otp = random.randint(1000, 9999)
            msz = "Halo {} \n\n{} adalah One Time Password (OTP) atau kode verifikasimu untuk ubah password. \nMohon jangan disebarluaskan. Jika itu bukan kamu mohon abaikan saja. Terima kasih:D \n\nSalam, \n\nPBP-F06".format(
                user.username, otp)
            try:
                email = EmailMessage("Account Verification", msz, to=[user.email])
                email.send()
                return JsonResponse({"status": "sent", "email": user.email, "rotp": otp})
            except:
                return JsonResponse({"status": "error", "email": user.email})
        except:
            return JsonResponse({"status": "failed"})