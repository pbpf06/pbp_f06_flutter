from django.db import models

# Create your models here.


class Question(models.Model):
    username = models.CharField(max_length=100)
    question = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
