from django.urls import path
from .views import addQuestion, index

urlpatterns = [
    # path('', index, name='index'),
    path('', addQuestion, name='add-question')
]
