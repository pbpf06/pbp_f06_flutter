from django import forms
from .models import Question
# Create your forms here.


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ("username", "question")
