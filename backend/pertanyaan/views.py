from django.http.response import JsonResponse
from django.shortcuts import render
from .forms import QuestionForm
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.


@csrf_exempt
def index(request):
    return render(request, 'index.html')


@csrf_exempt
def addQuestion(request):
    data = {}
    if request.method == "POST":
        data = json.loads(request.body)
        form = QuestionForm(data)

        if form.is_valid():
            form.save()
            data['status'] = True
            data['message'] = "Pertanyaan berhasil disampaikan."
        else:
            data['status'] = False
            data['message'] = "Pertanyaan gagal disampaikan."
    return JsonResponse(data)
