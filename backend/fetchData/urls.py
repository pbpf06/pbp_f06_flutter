from django.urls import path
from .views import fetchData

urlpatterns = [
    path('fetchData/', fetchData, name='fetchData')
]
