from django.http.response import JsonResponse
from django.views.decorators import csrf
import requests
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


@csrf_exempt
def fetchData(request):
    response = {}
    if request.method == "POST":
        url = json.loads(request.body)['url']
        response = requests.get(
            url).json()
    return JsonResponse(response)
