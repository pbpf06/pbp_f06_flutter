# Generated by Django 4.0 on 2021-12-31 15:53

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=50)),
                ('text', models.TextField(verbose_name='text')),
                ('viewed', models.PositiveBigIntegerField(blank=True, default=0, editable=False)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles', to='auth.user', verbose_name='author')),
                ('liked', models.ManyToManyField(blank=True, null=True, related_name='liked', to=settings.AUTH_USER_MODEL, verbose_name='liked')),
            ],
            options={
                'ordering': ['created', 'title'],
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now=True)),
                ('text', models.TextField(verbose_name='content')),
                ('article', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='berita.article', verbose_name='article')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='comments', to='auth.user', verbose_name='author')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
