from django.http import JsonResponse, Http404
from .models import Article, Comment
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count

def index(request):
    param = request.GET
    if 'num_commented' in param['ordering']:
        articles = Article.objects.annotate(
            num_commented=Count('comments')).order_by("-"+param['ordering'])
    elif 'num_liked' in param['ordering']:
        articles = Article.objects.annotate(
            num_liked=Count('liked')).order_by("-"+param['ordering'])
    else:
        articles = Article.objects.order_by("-"+param['ordering'])

    data = {'data': [], 'num_of_element': len(Article.objects.all())}
    page_num = int(param['page_num']) #1-indexing
    showing = int(param['showing'])

    try:
        for i in range(showing*(page_num-1), showing*page_num):
            article = articles[i]
            data['data'] += [{
                'author': article.author.username,
                'title': article.title.title(),
                'date': article.created.strftime('%I:%M %p %d/%m/%Y'),
                'liked': len(article.liked.all()),
                'comments': len(article.comments.all()),
                'viewed': article.viewed,
                'id': article.id,
            }]
    except IndexError:
        pass
    return JsonResponse(data)

def rekomendasi(request):
    articles = Article.objects.order_by('?')
    if len(articles) >5:
        articles = articles[:5]
    data = {'data':list()}
    for article in articles:
        data['data'] +=[{
            'author': article.author.username,
            'title': article.title.title(),
            'date': article.created.strftime("%d-%m-%Y"),
            'id': article.id,
        }]
    return JsonResponse(data)


def view_berita(request):
    param = request.GET

    try:
        temp = int(param['id'])
        article = Article.objects.get(pk=temp)
    except Exception as e:
        raise Http404('site does not exist')
    
    data = {
        'author': article.author.username,
        'title': article.title.title(),
        'date': article.created.strftime('%A, %d %B %Y'),
        'viewed': article.viewed,
        'like_num': len(article.liked.all()),
        'text': article.text,
        'comment_num': bool(len(article.comments.all())),
    }
    if 'user-id' not in param or int(param['user-id'])==0:
        data['liked'] = False
    else:
        user = User.objects.get(pk=int(param['user-id']))
        try:
            article.liked.get(id=user.id)
            data['liked'] = True
        except:
            data['liked'] = False
    return JsonResponse(data)

    

@csrf_exempt
def comment(request):
    param = request.GET

    try:
        article = Article.objects.get(pk=int(param['id']))
    except:
        raise Http404('<h1> site does not exist</h1>')


    if request.method =="POST":
        user = User.objects.get(pk=int(request.POST['user-id']))
        txt = request.POST['text']
        comment = Comment(author=user, text=txt, article=article)
        comment.save()
        return JsonResponse({
            'author':comment.author.username,
            'date': comment.created.strftime("%A, %d %B %Y"),
            'text': comment.text,
        })

    elif request.method =="GET":
        data = {'data':list()}
        
        for comment in article.comments.all():
            data['data'] += [{
                'author': comment.author.username,
                'date': comment.created.strftime("%A, %d %B %Y"),
                'text': comment.text,
            }]
        
        return JsonResponse(data)

@csrf_exempt
def like(request):
    param = request.GET

    try:
        article = Article.objects.get(pk=int(param['id']))
        user = User.objects.get(pk=int(param['user-id']))
    except:
        raise Http404('<h1> site does not exist</h1>')
    
    liked = int(param['liked'])
    if liked:
        article.liked.add(user)
    else:
        article.liked.remove(user)

    article.save()
    return JsonResponse({'Success':True})