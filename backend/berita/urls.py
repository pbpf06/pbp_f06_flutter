from django.urls import path
from .views import index, rekomendasi, view_berita, comment, like

urlpatterns = [
    path('', index, name="index"),
    path('rekomendasi', rekomendasi, name="get_rekomendasi_berita"),
    path('view', view_berita, name="view_berita"),
    path('comment', comment, name="comment"),
    path('like', like, name="like"),
]