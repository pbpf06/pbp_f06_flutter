from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):

    author = models.ForeignKey(User, verbose_name ="author", on_delete=models.SET_NULL, related_name="articles", null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False, blank=True)
    title = models.CharField(max_length=50)
    text = models.TextField(verbose_name="text")
    liked = models.ManyToManyField(User, verbose_name="liked", related_name="liked", null=True, blank=True)
    viewed= models.PositiveBigIntegerField(editable=False, blank=True, default=0)
    
    class Meta:
        ordering = ['created', "title"]
class Comment(models.Model):

    author = models.ForeignKey(User, verbose_name ="author", on_delete=models.SET_NULL, related_name="comments", null=True)
    created = models.DateTimeField(auto_now=True, editable=False, blank=True)
    text = models.TextField(verbose_name="content")
    article = models.ForeignKey(Article, verbose_name="article", on_delete=models.CASCADE, null=True, related_name="comments")

    class Meta:
        ordering = ['created']
