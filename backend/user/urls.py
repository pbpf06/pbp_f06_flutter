from django.urls import path
from .views import login, auth, register, getData, setting, delete_user, change_password

urlpatterns = [
    path('login/', login, name='login'),
    path('auth/', auth, name='auth'),
    path('register/', register, name='register'),
    path('getData/<id>', getData, name='getData'),
    path('setting/', setting, name='setting'),
    path('delete-user/', delete_user, name='delete-user'),
    path('password/', change_password, name='password'),
]
