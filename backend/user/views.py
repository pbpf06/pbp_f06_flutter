from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, authenticate, logout
import json
import secrets
from django.contrib.auth.models import User
from .models import Profile
from .forms import NewProfileForm, NewUserForm, ProfileUpdateForm
# Create your views here.


def createSession(username):
    data = {}
    user = User.objects.get(username=username)
    data['user'] = user
    data['user_id'] = user.pk
    data['session_id'] = secrets.token_urlsafe(50)
    return data


@csrf_exempt
def login(request):
    data = {"status": "Login gagal.", "success": False}
    if request.method == "POST":
        user_data = json.loads(request.body)
        username = user_data['username']
        password = user_data['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            # create new session_id every log in
            session_id = secrets.token_urlsafe(50)
            user_session = Profile.objects.filter(user=user)
            user_session.update(session_id=session_id)

            data['session_id'] = session_id
            data['user_id'] = user.pk
            data['status'] = "Login berhasil."
            data['success'] = True
    return JsonResponse(data)


@csrf_exempt
def register(request):
    data = {"status": "Registrasi gagal.", "success": False}
    if request.method == "POST":
        register_data = json.loads(request.body)
        form = NewUserForm(register_data)
        if form.is_valid():
            form.save()
            session_data = createSession(register_data['username'])
            profile = NewProfileForm(session_data)
            if profile.is_valid():
                profile.save()
            data['status'] = "Registrasi berhasil."
            data['session_id'] = session_data['session_id']
            data['user_id'] = session_data['user_id']
            data['success'] = True
    return JsonResponse(data)


@csrf_exempt
def auth(request):
    data = {"is_authenticated": False}
    if request.method == "POST":
        session_data = json.loads(request.body)
        user_id = session_data['user_id']
        user = User.objects.get(pk=user_id)
        user_session = Profile.objects.get(user=user)
        if user_session.session_id == session_data['session_id']:
            data['is_authenticated'] = True
    return JsonResponse(data)


@csrf_exempt
def getData(request, id):
    data = {'success': False}
    if request.method == "GET":
        user = User.objects.get(pk=id)
        print(user)
        profile = Profile.objects.get(user=user)
        data['username'] = user.username
        data['email'] = user.email
        data['first_name'] = profile.first_name
        data['last_name'] = profile.last_name
        data['gender'] = profile.gender
        data['date_of_birth'] = profile.date_of_birth
        data['number_phone'] = profile.number_phone
        data['province'] = profile.province
        data['city'] = profile.city
        data['district'] = profile.district
        data['bio'] = profile.bio
        data['success'] = True
    return JsonResponse(data)


@csrf_exempt
def setting(request):
    data = {}
    if request.method == 'POST':
        data = json.loads(request.body)
        form = ProfileUpdateForm(data)

        user = User.objects.get(pk=data['user_id'])
        profile = Profile.objects.get(user=user)

        if form.is_valid():
            profile.first_name = data['first_name']
            profile.last_name = data['last_name']
            profile.gender = data['gender']
            profile.date_of_birth = data['date_of_birth']
            profile.number_phone = data['number_phone']
            profile.province = data['province']
            profile.city = data['city']
            profile.district = data['district']
            profile.bio = data['bio']
            profile.save()

            data['message'] = "Berhasil mengganti profil."
            data['success'] = True
        else:
            data['message'] = "Mohon masukkan data yang benar."
    return JsonResponse(data)

@csrf_exempt
def delete_user(request):
    data = {}
    data = json.loads(request.body)
    user = User.objects.get(pk=data['user_id'])
    user.delete()
    data['success'] = True
    return JsonResponse(data)

@csrf_exempt
def change_password(request):
    data = {}
   
    if request.method == "POST":
        data = json.loads(request.body)
        user = User.objects.get(pk=data['user_id'])
        current = data["cpwd"]
        new_pas = data["npwd"]

        check = user.check_password(current)

        if check == True and current == new_pas:
            data["msz"] = "Password yang ingin diubah sama. Mohon periksa kembali."

        else:
            if check == True:
                user.set_password(new_pas)
                user.save()
                data["msz"] = "Berhasil mengganti password."
                data["success"] = "true"

            else:
                data["msz"] = "Password yang dimasukkan salah."
    return JsonResponse(data)
